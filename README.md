# PostgreSQL kustomization

Deploy postgresql in kubernetes as a simple non-redundant instance.

## Deployment of your own branch

Here's an example of how to deploy into your own kubernetes based on this branch.

### Clone the git repo

    git clone https://gitlab.com/stemid/postgresql-server

### Create your own branch for your environment

    git checkout -B my-env

### Create your own overlay

    mkdir -p kustomize/overlays/my-env/configs
    cp kustomize/base/kustomization.yaml kustomize/overlays/my-env

#### Edit kustomization.yaml

Here is an example that creates your own users when the service first starts.

```yaml
resources:
  - ../../base

patchesStrategicMerge:
  - deployment.yaml

configMapGenerator:
  - name: postgresql-init-script
    files:
      - configs/init-user-db.sh
```

#### Create your own deployment.yaml patch

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: postgresql

spec:
  template:
    spec:
      containers:
      - name: postgresql
        env:
          - name: POSTGRES_PASSWORD
            value: "My own password."
        volumeMounts:
          - mountPath: /docker-entrypoint-initdb.d/init-user-db.sh
            name: init-script
            subPath: init-user-db.sh

      volumes:
        - name: init-script
          configMap:
            name: postgresql-init-script
```

#### Create the init-user-db.sh script

```bash
#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER grafana;
    CREATE DATABASE grafana;
    GRANT ALL PRIVILEGES ON DATABASE grafana TO grafana;
    ALTER USER grafana WITH PASSWORD 'My own password.';
EOSQL
```

### Deploy to your kubernetes

    pushd kustomize/overlays/my-env
    kustomize build . | kubectl -n postgresql apply -f -

* Obviously I would recommend testing first to ensure kustomize build works and your kubectl needs to be setup with the right context, and you need to create the postgresql namespace.
* By default in k3s this will map up a host path for the pod to use as data directory. Works fine for homelab purposes.
